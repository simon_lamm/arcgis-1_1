define([
    "dijit/_WidgetBase",
    "dijit/_OnDijitClickMixin",
    "dijit/_TemplatedMixin",

    "dojo/Evented",
    "dojo/_base/declare",
    "dojo/_base/lang",

    "dojo/on",

    "dojo/dom",
    "dojo/dom-construct",
    "esri/dijit/InfoWindow",
    "esri/InfoTemplate",

    "esri/request"
    ], function (_WidgetBase, _OnDijitClickMixin, _TemplatedMixin, Evented, declare, lang, on, dom, domConstruct, InfoWindow, InfoTemplate, esriRequest) { 
        var Widget = declare("esri.dijit.WeatherWindow", [_WidgetBase, _TemplatedMixin, Evented], { 
            templateString: "./templates/weatherWidget",

            // default options
            options: {
                map: null,
                width: 250, 
                height: 150,
                anchor: InfoWindow.ANCHOR_LOWERRIGHT            
            },

            // lifecycle: 1
            constructor: function(options) {
                // mix in settings and defaults
                var defaults = lang.mixin({}, this.options, options);
                // store localized strings
                this._i18n = i18n;
                // properties
                this.set("map", defaults.map);
                this.set("width", defaults.width);
                this.set("height", defaults.height);
                this.set("anchor", defaults.anchor);
                // classes
                this._css = {
                    container: "windowContainer",
                    weatherWindow: "weatherWindow",
                    place: "place",
                    weatherForceast: "weatherForceast"
                };
            }, 

            // bind listener for button to action
            postCreate: function() {
                this.inherited(arguments);
                this.own(
                    on(this.map.id, a11yclick, lang.hitch(this, this.home))
                );
            },

            // start widget. called by user
            startup: function() {
                // map not defined
                if (!this.map) {
                    this.destroy();
                    console.log('WeatherWindow::map required');
                }
                // when map is loaded
                if (this.map.loaded) {
                    this._init();
                } else {
                    on.once(this.map, "load", lang.hitch(this, function() {
                        this._init();
                    }));
                }
            },

            // connections/subscriptions will be cleaned up during the destroy() lifecycle phase
            destroy: function() {
                this.inherited(arguments);
            },

            /* ---------------- */
            /* Public Functions */
            /* ---------------- */
            openWeatherWindow: function() {
                //TODO: get the coordinates of the click

                // start geocoder request
                var geocoderUrl = "";
                var geocoderRequest = esriRequest({
                    url: geocoderUrl,
                    //TODO: define request options
                });

                geocoderRequest.then(
                    function(geocoderResponse) {
                        //TODO: take geocoder response

                        // start weather service request
                        var weatherServiceUrl = "";
                        var weatherServiceRequest = esriRequest({
                            url: weatherServiceUrl,
                            //TODO: define request options
                        });

                        weatherServiceRequest.then(
                            function(weatherServiceReponse) {
                                //TODO: take response of weather service and display in info window
                            }, function(err) {
                                console.error("Error fetching weather service data: ", err.message);
                            }
                        )
                    }, function(err) {
                        console.error("Error fetching geocoder data: ", err.message);
                    }
                )
                //TODO: get geocoder data
                //TODO: get weather data
                this._showWeatherWindow();
            },
            closeWeatherWindow: function() {
                this._hideWeatherWindow();
            },

            /* ---------------- */
            /* Private Functions */
            /* ---------------- */
            _init: function() {
                this.infoWindow = new InfoWindow({
                    domNode: domConstruct.create("div", null, dom.byId(this.map.id))
                });

                this.infoWindow.setFixedAnchor(this.anchor);

                this.map.setInfoWindow(infoWindow);
                this.map.infoWindow.resize(this.width, this.height);
            },
            _showWeatherWindow: function() {
                this.infoWindow.hide();
            },
            _hideWeatherWindow: function() {
                this.infoWindow.show();
            }
        });
        if (has("extend-esri")) {
            lang.setObject("dijit.WeatherWindow", Widget, esriNS);
        }
        return Widget;
    }
);