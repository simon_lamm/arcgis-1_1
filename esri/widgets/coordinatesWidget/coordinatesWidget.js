define([
    "dijit/_WidgetBase",
    "dijit/_OnDijitClickMixin",
    "dijit/_TemplatedMixin",

    "dojo/Evented",
    "dojo/_base/declare",
    "dojo/_base/lang",

    "dojo/on",

    "dojo/dom",
    "dojo/dom-construct",
    "esri/dijit/InfoWindow",
    "esri/InfoTemplate"
    ], function (_WidgetBase, _OnDijitClickMixin, _TemplatedMixin, Evented, declare, lang, on, dom, domConstruct, InfoWindow, InfoTemplate) { 
        var Widget = declare("esri.dijit.CoordinatesWindow", [_WidgetBase, _TemplatedMixin, Evented], { 
            templateString: "./templates/coordinatesWidget",

            // default options
            options: {
                map: null,
                width: 250, 
                height: 100,
                anchor: InfoWindow.ANCHOR_UPPERRIGHT
            },

            // lifecycle: 1
            constructor: function(options) {
                // mix in settings and defaults
                var defaults = lang.mixin({}, this.options, options);
                // store localized strings
                this._i18n = i18n;
                // properties
                this.set("map", defaults.map);
                this.set("width", defaults.width);
                this.set("height", defaults.height);
                this.set("anchor", defaults.anchor);
                // classes
                this._css = {
                    container: "windowContainer",
                    coordinatesWindow: "coordinatesWindow",
                    longitude: "longitude",
                    latitude: "latitude"
                };
            }, 

            // bind listener for button to action
            postCreate: function() {
                this.inherited(arguments);
                this.own(
                    on(this.map.id, a11yclick, lang.hitch(this, this.home))
                );
            },

            // start widget. called by user
            startup: function() {
                // map not defined
                if (!this.map) {
                    this.destroy();
                    console.log('CoordinatesWindow::map required');
                }
                // when map is loaded
                if (this.map.loaded) {
                    this._init();
                } else {
                    on.once(this.map, "load", lang.hitch(this, function() {
                        this._init();
                    }));
                }
            },

            // connections/subscriptions will be cleaned up during the destroy() lifecycle phase
            destroy: function() {
                this.inherited(arguments);
            },

            /* ---------------- */
            /* Public Functions */
            /* ---------------- */
            openCoordinatesWindow: function() {
                //TODO: get the coordinates of the click
                //TODO: show coordinates info window
            },
            closeCoordinatesWindow: function() {
                //TODO: close the coordinates info window
            },

            /* ---------------- */
            /* Private Functions */
            /* ---------------- */
            _init: function() {
                this.infoWindow = new InfoWindow({
                    domNode: domConstruct.create("div", null, dom.byId(this.map.id))
                });

                this.infoWindow.setFixedAnchor(this.anchor);

                this.map.setInfoWindow(infoWindow);
                this.map.infoWindow.resize(this.width, this.height);
            },
            _showCoordinatesWindow: function() {
                this.infoWindow.hide();
            },
            _hideCoordinatesWindow: function() {
                this.infoWindow.show();
            }
        });
        if (has("extend-esri")) {
            lang.setObject("dijit.CoordinatesWindow", Widget, esriNS);
        }
        return Widget;
    }
);