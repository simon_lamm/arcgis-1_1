define([
    "dojo/_base/declare", 
    "dojo/_base/lang", 
    "esri/map", 
    "esri/InfoTemplate", 
    "esri/layers/FeatureLayer"],
    function(declare, lang, Map, InfoTemplate, FeatureLayer) { 
        declare(null, { 
            divId: null,
            basemap: null,
            startLongitude: null,
            startLatitude: null,
            startZoom: null,

            // Constructor of the basis map. The div id is required. Every other option is optional.
            constructor: function(options) {
                if(!options.divId) {
                    //TODO: does this work?
                    this.destroy();
                    console.log('Basic Map::div id is required');
                } else {
                    this.divId = options.divId;
                }
                this.basemap = options.basemap || "dark-gray-vector"; // default basemap is dark-gray-vector as given in the task
                this.startLongitude = options.startLongitude || 7.6525050; // default longitude is position of con terra
                this.startLatitude = options.startLatitude || 51.9350372; // default latitude is position of con terra
                this.startZoom = options.startZoom || 18 // default zoom is 13

                //TODO: is this the right place to init map?
                this.map = new Map(this.divId, {
                    basemap: this.basemap,
                    center: [this.startLongitude, this.startLatitude],
                    zoom: this.startZoom
                });
            },

            // Add a layer to the map. 
            addLayer: function(layer) {
                this.map.addLayer(layer);
            },

            // Create a info template with the given info header and info content
            createInfoTemplate: function(infoHeader, infoContent) {
                return new InfoTemplate(infoHeader, infoContent);
            },

            // Create a feature layer with the given layer id and options
            createFeatureLayer: function(layerId, outFields, infoTemplate) {
                return new FeatureLayer(featureLayerId, {
                    outFields: outFields,
                    infoTemplate: infoTemplate
                });
            }
         });
    }
  );