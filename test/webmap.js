// Dev dependencies
let chai = require("chai");
let should = chai.should();
let expect = chai.expect;
let chaiHttp = require("chai-http");
let server = require('../app');
let rewire = require("rewire");
let webmap = rewire("../public/javascripts/webmap");

chai.use(chaiHttp);

//TODO: create (fake?) DOM with esri scripts so this tests are working?

// Function not working because DOM does not exists
describe("Create webmap", async () => {
    var createWebmap = webmap.__get__("initMap");
    var webmapObject = await createWebmap("map", "dark-gray-vector", 7.6525050, 51.9350372, 18);
    it("Should create an esri webmap", (done) => {
        expect(webmapObject.__proto__.declaredClass).to.equal("esri.Map");
        done();
    });
});

// Function not working because DOM does not exists
describe("Create info template", async () => {
    var createInfoTemplate = webmap.__get__("getInfoTemplate");
    var infoTemplateObject = await createInfoTemplate("${CITY_NAME}", "<b>Population: </b> ${POP}");
    it("Should create an esri info template", (done) => {
        expect(infoTemplateObject.__proto__.declaredClass).to.equal("esri.InfoTemplate");
        done();
    });
});

// Function not working because DOM does not exists
describe("Create feature layer", async () => {
    var createFeatureLayer = webmap.__get__("getFeatureLayer");
    var featureLayerObject = await createFeatureLayer("https://services.arcgis.com/P3ePLMYs2RVChkJx/ArcGIS/rest/services/World_Cities/FeatureServer/0", ["CITY_NAME", "POP"], infoTemplate);
    it("Should create an esri info template", (done) => {
        expect(featureLayerObject.__proto__.declaredClass).to.equal("esri.layers.FeatureLayer");
        done();
    });
});