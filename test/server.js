// Dev dependencies
let chai = require("chai");
let chaiHttp = require("chai-http");
let server = require('../app');
let should = chai.should();
let expect = chai.expect;

chai.use(chaiHttp);
// Test '/' route
describe('/ index route', () =>  {
    it('it should return 200 status code', (done) => {
        chai.request(server)
            .get('/')
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });
    // Test if map element exists
    it('it should have a map element', (done) => {
        chai.request(server)
            .get('/')
            .end((err, res) => {
                expect(res.text).to.contain('id="map"');
                done();
            });
    });
});