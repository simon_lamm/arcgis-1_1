## Description

This is a Node.js web app to show weather data on a map.
The geo data is provided by the ArcGIS API. 

---

## Requirements

Node.js needs to be installed to run this web app (tested on version 12.11.1).

---

## Installation

1. Clone this repository.
2. Run ```npm install``` in the repository folder to install necessary packages.
3. Run ```npm start``` to start the webapp.
4. Go to **localhost** in your browser to open the webapp.

You can now use the web app in your browser. To stop the app kill the Node.js task in your terminal.

---

## Important

The web app is running on an old version of the ArcGIS API. 
Documenation can be found here: [https://developers.arcgis.com/javascript/3/](https://developers.arcgis.com/javascript/3/)

---

## Copyright

© Copyright by Simon Lamm