/**
 * Function to initialise the webmap and returning it.
 * 
 * @param {string} divId - The id of the div which renders the webmap
 * @param {string} basemap - The type of the basemap
 * @param {float} longitude - Degree of longitude for starting point
 * @param {float} latitude - Degree of lateitude for starting point
 * @param {float} zoom - Level of zoom for starting point
 * @return {esri/Map}
 */
function initMap(divId, basemap, longitude, latitude, zoom) {
    return new Promise(resolve => {
        require([
            "esri/map"
        ], function(Map) {
            resolve(new Map(divId, {
                basemap: basemap,
                center: [longitude, latitude],
                zoom: zoom
            }));
        });
    });
}

/**
 * Function returning an esri info template.
 * 
 * @param {string} infoHeader - The title of the info template
 * @param {string} infoContent - The content of the info template
 * @return {esri/InfoTemplate}
 */
function getInfoTemplate(infoHeader, infoContent) {
    return new Promise(resolve => {
        require([
            "esri/InfoTemplate"
        ], function (InfoTemplate) {
            resolve(new InfoTemplate(infoHeader, infoContent));
        });
    });
}

/**
 * Function returning the specified feature layer.
 * 
 * @param {string} featureLayerId - The id of the feature layer
 * @param {Array.<string>} outFields - Fields of the layer which are needed for the info template
 * @param {esri/InfoTemplate} infoTemplate - The info template for this layer
 * @return {esri/layers/FeatureLayer}
 */
function getFeatureLayer(featureLayerId, outFields, infoTemplate) {
    return new Promise(resolve => {
        require([
            "esri/layers/FeatureLayer"
        ], function(FeatureLayer) { 
            resolve(new FeatureLayer(featureLayerId, {
                outFields: outFields,
                infoTemplate: infoTemplate
            }));
        });
    });
}

/**
 * Function to add info window to arcgis webmap
 * 
 * @return {esri/dijit/InfoWindow}
 */
function getInfoWindow() {
    return new Promise(resolve => {
        require([
            "dojo/dom",
            "dojo/dom-construct",
            "esri/dijit/InfoWindow",
            "esri/InfoTemplate"
        ], function(dom, domConstruct, InfoWindow, InfoTemplate) {
            // init info window
            var infoWindow = new InfoWindow({
                domNode: domConstruct.create("div", null, dom.byId("map"))
            });

            //TODO: anchor not working?
            infoWindow.setFixedAnchor(InfoWindow.ANCHOR_UPPERRIGHT);
            
            resolve(infoWindow);
        });
    });
}

/**
 * Function to add a info window to a map with given parameters
 * 
 * @param {esri/Map} map The map for the info window.
 * @param {esri/dijit/InfoWindow} infoWindow The info window which gets added to the given map.
 */
function setCustomInfoWindow(map, infoWindow) {
    map.setInfoWindow(infoWindow);
    map.infoWindow.resize(200, 75);

    infoWindow.setTitle("Coordinates");
}

/**
 * Function to start the weather webmap app.
 */
async function startWeatherMap() {
    // init esri webmap
    var map = await initMap("map", "dark-gray-vector", 7.6525050, 51.9350372, 18);

    // add world cities feature layer to map
    var infoTemplate = await getInfoTemplate("${CITY_NAME}", "<b>Population: </b> ${POP}");
    var featureLayer = await getFeatureLayer("https://services.arcgis.com/P3ePLMYs2RVChkJx/ArcGIS/rest/services/World_Cities/FeatureServer/0", ["CITY_NAME", "POP"], infoTemplate);
    map.addLayer(featureLayer);

    //TODO: remove this - not working properly
    // only added for proove of work of info window widget
    var infoWindow = await getInfoWindow();
    setCustomInfoWindow(map, infoWindow);

    console.log(map);
    console.log(infoTemplate);
    console.log(featureLayer);
    console.log(infoWindow);
}

startWeatherMap();


/*
    TODOS when there is time and classes and widgets would work
*/

//TODO: use created baseMap.js class instead of methods in this file

//TODO: add coordinates widget

//TODO: add weather widget